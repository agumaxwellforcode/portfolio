import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import VueTypedJs from 'vue-typed-js';
import axios from 'axios'
import router from './router'

Vue.use(VueTypedJs)
Vue.use(axios)

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
